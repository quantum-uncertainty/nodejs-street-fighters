const { UserRepository } = require('../repositories/userRepository');
const { NotFoundException } = require('../helpers/customExceptions');

class UserService {
    getAll() {
        return UserRepository.getAll();
    }

    getById(id) {
        const user = this.search({ id });
        if (!user) {
            throw new NotFoundException(`User with id '${id}' was not found`);
        }
        return user;
    }

    create(data) {
        const formattedData = formatData(data);
        return UserRepository.create(formattedData);
    }

    update(id, data) {
        if (!this.getById(id)) {
            throw new NotFoundException(`User with id '${id}' was not found, can't be updated`);
        }
        const formattedData = formatData(data);
        const updatedUser = UserRepository.update(id, formattedData);
        return updatedUser;
    }

    delete(id) {
        if (!this.getById(id)) {
            throw new NotFoundException(`User with id '${id}' was not found, can't be deleted`);
        }
        const deletedUser = UserRepository.delete(id);
        return deletedUser;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

function formatData(data) {
    const formattedData = { ...data };
    if (data.firstName) {
        formattedData.firstName = data.firstName.trim();
    }
    if (data.lastName) {
        formattedData.lastName = data.lastName.trim();
    }
    if (data.email) {
        formattedData.email = data.email.trim().toLowerCase();
    }
    if (data.phoneNumber) {
        formattedData.phoneNumber = data.phoneNumber.trim();
    }
    return formattedData;
}

module.exports = new UserService();
