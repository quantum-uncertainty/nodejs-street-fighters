const UserService = require('./userService');
const { NotFoundException, BadRequestException } = require('../helpers/customExceptions');

class AuthService {
    login(userData) {
        const { email, password } = userData;
        const user = UserService.search({ email: email.trim().toLowerCase() });
        if (!user) {
            throw new NotFoundException(`User with email '${email}' not found`);
        }
        if (user.password !== password) {
            throw new BadRequestException('Wrong password');
        }
        return user;
    }
}

module.exports = new AuthService();
