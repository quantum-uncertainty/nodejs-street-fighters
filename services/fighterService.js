const { FighterRepository } = require('../repositories/fighterRepository');
const { NotFoundException } = require('../helpers/customExceptions');

class FighterService {
    getAll() {
        return FighterRepository.getAll();
    }

    getById(id) {
        const fighter = this.search({ id });
        if (!fighter) {
            throw new NotFoundException(`Fighter with id '${id}' was not found`);
        }
        return fighter;
    }

    create(fighter) {
        return FighterRepository.create({ ...fighter, health: 100 });
    }

    update(id, data) {
        if (!this.getById(id)) {
            throw new NotFoundException(`Fighter with id '${id}' was not found, can't be updated`);
        }
        return FighterRepository.update(id, data);
    }

    delete(id) {
        if (!this.getById(id)) {
            throw new NotFoundException(`Fighter with id '${id}' was not found, can't be deleted`);
        }
        return FighterRepository.delete(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();
