const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { errorMiddleware } = require('../middlewares/error.middleware');
const {
    createFighterValid,
    updateFighterValid,
} = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', function getFighters(req, res, next) {
    try {
        const fighters = FighterService.getAll();
        res.data = fighters;
        next();
    } catch (err) {
        next(err);
    }
});

router.get('/:id', function getFighterById(req, res, next) {
    const { id } = req.params;
    try {
        const fighter = FighterService.getById(id);
        res.data = fighter;
        next();
    } catch (err) {
        next(err);
    }
});

router.post('/', createFighterValid, function createFighter(req, res, next) {
    const data = req.body;
    try {
        const newFighter = FighterService.create(data);
        res.data = newFighter;
        next();
    } catch (err) {
        next(err);
    }
});

router.put('/:id', updateFighterValid, function updateFighter(req, res, next) {
    const { id } = req.params;
    const data = req.body;
    try {
        const updatedFighter = FighterService.update(id, data);
        res.data = updatedFighter;
        next();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', function deleteFighter(req, res, next) {
    const { id } = req.params;
    try {
        const deletedFighter = FighterService.delete(id);
        res.data = deletedFighter;
        next();
    } catch (err) {
        next(err);
    }
});

router.use(responseMiddleware);
router.use(errorMiddleware);

module.exports = router;
