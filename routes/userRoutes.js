const { Router } = require('express');
const UserService = require('../services/userService');
const {
    createUserValid,
    updateUserValid,
} = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { errorMiddleware } = require('../middlewares/error.middleware');

const router = Router();

router.get('/', function getUsers(req, res, next) {
    try {
        const users = UserService.getAll();
        res.data = users;
        next();
    } catch (err) {
        next(err);
    }
});

router.get('/:id', function getUserById(req, res, next) {
    const { id } = req.params;
    try {
        const user = UserService.getById(id);
        res.data = user;
        next();
    } catch (err) {
        next(err);
    }
});

router.post('/', createUserValid, function createUser(req, res, next) {
    const data = req.body;
    try {
        const newUser = UserService.create(data);
        res.data = newUser;
        next();
    } catch (err) {
        next(err);
    }
});

router.put('/:id', updateUserValid, function updateUser(req, res, next) {
    const { id } = req.params;
    const data = req.body;
    try {
        const updatedUser = UserService.update(id, data);
        res.data = updatedUser;
        next();
    } catch (err) {
        next(err);
    }
});

router.delete('/:id', function deleteUser(req, res, next) {
    const { id } = req.params;
    try {
        const deletedUser = UserService.delete(id);
        res.data = deletedUser;
        next();
    } catch (err) {
        next(err);
    }
});

router.use(responseMiddleware);
router.use(errorMiddleware);

module.exports = router;
