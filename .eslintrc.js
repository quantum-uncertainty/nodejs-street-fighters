module.exports = {
    env: {
        commonjs: true,
        es2021: true,
        node: true,
    },
    extends: [
        'airbnb-base',
    ],
    parserOptions: {
        ecmaVersion: 12,
    },
    ignorePatterns: [
        'client/',
    ],
    rules: {
        indent: ['warn', 4],
        'arrow-parens': 'off',
        'class-methods-use-this': 'off',
        'max-len': ['warn', {
            code: 80,
            comments: 100,
            ignoreStrings: true,
            ignoreTemplateLiterals: true,
            ignoreRegExpLiterals: true,
        }],
        'no-use-before-define': ['error', 'nofunc'],
        'prefer-arrow-callback': ['warn', { allowNamedFunctions: true }],
    },
};
