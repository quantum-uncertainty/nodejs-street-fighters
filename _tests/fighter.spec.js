const chai = require('chai');
const chaiHttp = require('chai-http');
const { describe, it, beforeEach } = require('mocha');

const { expect } = chai;
chai.use(chaiHttp);

process.env.NODE_ENV = 'test';

const { app } = require('../index');
const { dbAdapter } = require('../config/db');

describe('Fighters CRUD API', () => {
    const BASE_URL = '/api/fighters';

    const testFighter = {
        id: '0',
        name: 'Ryu',
        power: 99,
        defense: 10,
    };

    beforeEach((done) => {
        const dbContext = dbAdapter.get('fighters');
        dbContext.remove().write();
        dbContext.push({ ...testFighter }).write();
        done();
    });

    // POST /api/fighters
    describe('CREATE', () => {
        const newFighter = {
            name: 'Ken',
            power: 99,
            defense: 10,
        };

        it('it should create a fighter', (done) => {
            chai.request(app)
                .post(BASE_URL)
                .send(newFighter)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(newFighter);
                    done();
                });
        });

        it('it should fail if the name is invalid', (done) => {
            const invalidRequest = { ...newFighter, name: '1234' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the power is invalid', (done) => {
            const invalidRequest = { ...newFighter, power: -1 };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the defense is invalid', (done) => {
            const invalidRequest = { ...newFighter, defense: 12 };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });
    });

    // GET /api/fighters
    describe('GET_ALL', () => {
        it('it should get all fighters', (done) => {
            chai.request(app)
                .get(BASE_URL)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body.length).to.be.equal(1);
                    expect(res.body[0]).to.include(testFighter);
                    done();
                });
        });
    });

    // GET /api/fighters/:id
    describe('GET_BY_ID', () => {
        it('it should get a fighter by id', (done) => {
            chai.request(app)
                .get(`${BASE_URL}/${testFighter.id}`)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(testFighter);
                    done();
                });
        });

        it('it should fail if fighter doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            chai.request(app)
                .get(url)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });
    });

    // PUT /api/fighters:id
    describe('UPDATE', () => {
        it('it should update a fighter by id', (done) => {
            const updateData = {
                power: 90,
                defense: 5,
            };
            chai.request(app)
                .put(`${BASE_URL}/${testFighter.id}`)
                .send(updateData)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    const response = res.body;
                    expect(response).to.be.a('object');
                    Object.keys(testFighter).forEach(key => {
                        if (key in updateData) {
                            expect(response[key]).to.be.equal(updateData[key]);
                        } else {
                            expect(response[key]).to.be.equal(testFighter[key]);
                        }
                    });
                    done();
                });
        });

        it('it should fail if fighter doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            const updateData = {
                power: 90,
                defense: 5,
            };
            chai.request(app)
                .put(url)
                .send(updateData)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should fail if the name is invalid', (done) => {
            const invalidRequest = { name: 'HI*(' };
            chai.request(app)
                .put(`${BASE_URL}/${testFighter.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the power is invalid', (done) => {
            const invalidRequest = { power: 101 };
            chai.request(app)
                .put(`${BASE_URL}/${testFighter.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the defense is invalid', (done) => {
            const invalidRequest = { defense: 0 };
            chai.request(app)
                .put(`${BASE_URL}/${testFighter.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });
    });

    // DELETE /api/fighters:id
    describe('DELETE', () => {
        it('it should delete a fighter by id', (done) => {
            chai.request(app)
                .delete(`${BASE_URL}/${testFighter.id}`)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(testFighter);
                    done();
                });
        });

        it('it should fail if fighter doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            chai.request(app)
                .delete(url)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });
    });
});
