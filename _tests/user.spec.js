const chai = require('chai');
const chaiHttp = require('chai-http');
const { describe, it, beforeEach } = require('mocha');

const { expect } = chai;
chai.use(chaiHttp);

process.env.NODE_ENV = 'test';

const { app } = require('../index');
const { dbAdapter } = require('../config/db');

describe('Users CRUD API', () => {
    const BASE_URL = '/api/users';

    const testUser = {
        id: '0',
        firstName: 'Name',
        lastName: 'Surname',
        email: 'email@gmail.com',
        phoneNumber: '+380951111111',
        password: 'qwerty',
    };

    beforeEach((done) => {
        const dbContext = dbAdapter.get('users');
        dbContext.remove().write();
        dbContext.push({ ...testUser }).write();
        done();
    });

    // POST /api/users
    describe('CREATE', () => {
        const newUser = {
            firstName: 'Name',
            lastName: 'Surname',
            email: 'other_email@gmail.com',
            phoneNumber: '+380952222222',
            password: 'password1',
        };

        it('it should create a user', (done) => {
            chai.request(app)
                .post(BASE_URL)
                .send(newUser)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(newUser);
                    done();
                });
        });

        it('it should fail if there is an id in the request body', (done) => {
            const invalidRequest = { ...newUser, id: '1' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if there is an invalid field in the request body', (done) => {
            const invalidRequest = { ...newUser, randomField: '' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if a required field is missing', (done) => {
            const { email, ...invalidRequest } = newUser;
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the first name is invalid', (done) => {
            const invalidRequest = { ...newUser, firstName: 'name123' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the last name is invalid', (done) => {
            const invalidRequest = { ...newUser, lastName: 'name!@#' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the email is invalid', (done) => {
            const invalidRequest = { ...newUser, email: 'email@hotmail.com' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the phone number is invalid', (done) => {
            const invalidRequest = { ...newUser, phoneNumber: '+330561234567' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the password is invalid', (done) => {
            const invalidRequest = { ...newUser, password: 'qw' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the email is already used', (done) => {
            const invalidRequest = { ...newUser, email: 'email@gmail.com' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the phone number is already used', (done) => {
            const invalidRequest = { ...newUser, phoneNumber: '+380951111111' };
            chai.request(app)
                .post(BASE_URL)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });
    });

    // GET /api/users
    describe('GET_ALL', () => {
        it('it should get all users', (done) => {
            chai.request(app)
                .get(BASE_URL)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('array');
                    expect(res.body.length).to.be.equal(1);
                    expect(res.body[0]).to.include(testUser);
                    done();
                });
        });
    });

    // GET /api/users/:id
    describe('GET_BY_ID', () => {
        it('it should get a user by id', (done) => {
            chai.request(app)
                .get(`${BASE_URL}/${testUser.id}`)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(testUser);
                    done();
                });
        });

        it('it should fail if user doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            chai.request(app)
                .get(url)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });
    });

    // PUT /api/users:id
    describe('UPDATE', () => {
        it('it should update a user by id', (done) => {
            const updateData = {
                email: 'new_email@gmail.com',
                phoneNumber: '+380950001122',
            };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(updateData)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    const response = res.body;
                    expect(response).to.be.a('object');
                    Object.keys(testUser).forEach(key => {
                        if (key in updateData) {
                            expect(response[key]).to.be.equal(updateData[key]);
                        } else {
                            expect(response[key]).to.be.equal(testUser[key]);
                        }
                    });
                    done();
                });
        });

        it('it should fail if user doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            const updateData = {
                firstName: 'New Name',
                email: 'new_email@gmail.com',
            };
            chai.request(app)
                .put(url)
                .send(updateData)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should fail if there is an id in the request body', (done) => {
            const invalidRequest = { id: '1' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if there is an invalid field in the request body', (done) => {
            const invalidRequest = { randomField: '' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the first name is invalid', (done) => {
            const invalidRequest = { firstName: 'name123' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the last name is invalid', (done) => {
            const invalidRequest = { lastName: 'name!@#' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the email is invalid', (done) => {
            const invalidRequest = { email: 'email@hotmail.com' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the phone number is invalid', (done) => {
            const invalidRequest = { phoneNumber: '+330561234567' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the password is invalid', (done) => {
            const invalidRequest = { password: 'qw' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the email is already used', (done) => {
            const invalidRequest = { email: 'eMaIl@GMAIL.com' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });

        it('it should fail if the phone number is already used', (done) => {
            const invalidRequest = { phoneNumber: '+380951111111' };
            chai.request(app)
                .put(`${BASE_URL}/${testUser.id}`)
                .send(invalidRequest)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });
    });

    // DELETE /api/users:id
    describe('DELETE', () => {
        it('it should delete a user by id', (done) => {
            chai.request(app)
                .delete(`${BASE_URL}/${testUser.id}`)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(testUser);
                    done();
                });
        });

        it('it should fail if user doesn\'t exist', (done) => {
            const url = `${BASE_URL}/fake_id`;
            chai.request(app)
                .delete(url)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });
    });
});
