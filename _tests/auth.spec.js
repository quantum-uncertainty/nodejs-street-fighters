const chai = require('chai');
const chaiHttp = require('chai-http');
const { describe, it, beforeEach } = require('mocha');

const { expect } = chai;
chai.use(chaiHttp);

process.env.NODE_ENV = 'test';

const { app } = require('../index');
const { dbAdapter } = require('../config/db');

describe('Auth API', () => {
    const testUser = {
        id: '0',
        firstName: 'Name',
        lastName: 'Surname',
        email: 'email@gmail.com',
        phoneNumber: '+380951111111',
        password: 'qwerty',
    };

    beforeEach((done) => {
        const dbContext = dbAdapter.get('users');
        dbContext.remove().write();
        dbContext.push({ ...testUser }).write();
        done();
    });

    // POST /api/auth/login
    describe('LOGIN', () => {
        const BASE_URL = '/api/auth/login';

        it('it should return the user', (done) => {
            const loginData = {
                email: 'email@gmail.com',
                password: 'qwerty',
            };
            chai.request(app)
                .post(BASE_URL)
                .send(loginData)
                .end((err, res) => {
                    expect(res).to.have.status(200);
                    expect(res.body).to.be.a('object');
                    expect(res.body).to.include(testUser);
                    done();
                });
        });

        it('it should fail if user doesn\'t exist', (done) => {
            const loginData = {
                email: 'wrong_email@gmail.com',
                password: 'qwerty',
            };
            chai.request(app)
                .post(BASE_URL)
                .send(loginData)
                .end((err, res) => {
                    expect(res).to.have.status(404);
                    done();
                });
        });

        it('it should fail if the password is invalid', (done) => {
            const loginData = {
                email: 'email@gmail.com',
                password: 'asdf',
            };
            chai.request(app)
                .post(BASE_URL)
                .send(loginData)
                .end((err, res) => {
                    expect(res).to.have.status(400);
                    done();
                });
        });
    });
});
