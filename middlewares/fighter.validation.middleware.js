const { fighter } = require('../models/fighter');
const {
    checkInvalidFields,
    checkRequiredFields,
    validateFields,
    validName,
} = require('../helpers/validationHelper');

const fighterValidator = {
    name: validName,
    power: validPower,
    defense: validDefense,
    health: () => true,
};

const createFighterValid = (req, res, next) => {
    try {
        const fighterData = { ...req.body, health: 100 };
        checkInvalidFields(fighterData, fighter);
        checkRequiredFields(fighterData, fighter);
        validateFields(fighterData, fighterValidator);
        next();
    } catch (err) {
        next(err);
    }
};

const updateFighterValid = (req, res, next) => {
    try {
        const fighterData = req.body;
        checkInvalidFields(fighterData, fighter);
        validateFields(fighterData, fighterValidator);
        next();
    } catch (err) {
        next(err);
    }
};

function validPower(power) {
    return (!Number.isNaN(power) && (power > 0 && power < 100));
}

function validDefense(defense) {
    return (!Number.isNaN(defense) && (defense >= 1 && defense <= 10));
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
