const { user } = require('../models/user');
const UserService = require('../services/userService');
const { BadRequestException } = require('../helpers/customExceptions');
const {
    checkInvalidFields,
    checkRequiredFields,
    validateFields,
    validName,
} = require('../helpers/validationHelper');

const userValidator = {
    firstName: validName,
    lastName: validName,
    email: validEmail,
    phoneNumber: validPhoneNumber,
    password: validPassword,
};

const createUserValid = (req, res, next) => {
    try {
        const userData = req.body;
        checkInvalidFields(userData, user);
        checkRequiredFields(userData, user);
        validateFields(userData, userValidator);
        const { email, phoneNumber } = userData;
        checkEmailAlreadyUsed(email);
        checkPhoneAlreadyUsed(phoneNumber);
        next();
    } catch (err) {
        next(err);
    }
};

const updateUserValid = (req, res, next) => {
    try {
        const userData = req.body;
        checkInvalidFields(userData, user);
        validateFields(userData, userValidator);
        const { email, phoneNumber } = userData;
        if (email) {
            checkEmailAlreadyUsed(email);
        }
        if (phoneNumber) {
            checkPhoneAlreadyUsed(phoneNumber);
        }
        next();
    } catch (err) {
        next(err);
    }
};

function checkEmailAlreadyUsed(email) {
    if (UserService.search({ email: email.trim().toLowerCase() })) {
        throw new BadRequestException(`Email '${email}' is already in use`);
    }
}

function checkPhoneAlreadyUsed(phoneNumber) {
    if (UserService.search({ phoneNumber: phoneNumber.trim() })) {
        throw new BadRequestException(`Phone number '${phoneNumber}' is already in use`);
    }
}

function validEmail(email) {
    const emailRegex = /^.+@gmail\.com$/i;
    return emailRegex.test(email.trim());
}

function validPhoneNumber(phoneNumber) {
    const phoneRegex = /^\+380\d{9}$/;
    return phoneRegex.test(phoneNumber.trim());
}

function validPassword(password) {
    return password.length >= 3;
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
