function NotFoundException(message) {
    this.message = message;
    this.statusCode = 404;
    this.name = 'NotFoundException';
}

function BadRequestException(message) {
    this.message = message;
    this.statusCode = 400;
    this.name = 'BadRequestException';
}

module.exports = {
    NotFoundException,
    BadRequestException,
};
