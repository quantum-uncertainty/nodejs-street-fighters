const { BadRequestException } = require('./customExceptions');

function checkInvalidFields(data, model) {
    Object.keys(data).forEach(field => {
        if (field === 'id') {
            throw new BadRequestException('Id is not allowed in the request body');
        }
        if (!(field in model)) {
            throw new BadRequestException(`Field '${field}' is invalid`);
        }
    });
}

function checkRequiredFields(data, model) {
    Object.keys(model).forEach(field => {
        if (field !== 'id' && !(field in data)) {
            throw new BadRequestException(`Field '${field}' is required`);
        }
    });
}

function validateFields(data, validator) {
    Object.keys(data).forEach(field => {
        const value = data[field];
        const isValid = validator[field];
        if (!isValid(value)) {
            throw new BadRequestException(`Invalid ${field}: '${data[field]}'`);
        }
    });
}

function validName(name) {
    const nameRegex = /^[a-zA-Z ]{1,100}$/;
    return nameRegex.test(name.trim());
}

module.exports = {
    checkInvalidFields,
    checkRequiredFields,
    validateFields,
    validName,
};
