const { v4 } = require('uuid');
const { dbAdapter } = require('../config/db');

class BaseRepository {
    constructor(collectionName) {
        this.dbContext = dbAdapter.get(collectionName);
        this.collectionName = collectionName;
    }

    generateId() {
        return v4();
    }

    getAll() {
        return this.dbContext.value();
    }

    getOne(search) {
        return this.dbContext.find(search).value();
    }

    create(data) {
        const newEntity = { ...data };
        newEntity.id = this.generateId();
        newEntity.createdAt = new Date();
        const list = this.dbContext.push(newEntity).write();
        return list.find(it => it.id === newEntity.id);
    }

    update(id, data) {
        const dataToUpdate = { ...data };
        dataToUpdate.updatedAt = new Date();
        return this.dbContext.find({ id }).assign(dataToUpdate).write();
    }

    delete(id) {
        return this.dbContext.remove({ id }).write()[0];
    }
}

exports.BaseRepository = BaseRepository;
