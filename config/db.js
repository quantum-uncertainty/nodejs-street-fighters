const path = require('path');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const dbName = process.env.NODE_ENV === 'test' ? 'test-database.json' : 'database.json';
const dbPath = `${path.resolve()}/${dbName}`;

const adapter = new FileSync(dbPath);

const dbAdapter = low(adapter);

const defaultDb = { users: [], fighters: [], fights: [] };

dbAdapter.defaults(defaultDb).write();

exports.dbAdapter = dbAdapter;
